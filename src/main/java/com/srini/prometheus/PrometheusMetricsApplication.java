package com.srini.prometheus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrometheusMetricsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrometheusMetricsApplication.class, args);
	}

}
